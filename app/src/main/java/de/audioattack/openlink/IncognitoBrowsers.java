package de.audioattack.openlink;

/**
 * Created by low012 on 23.10.17.
 */

public interface IncognitoBrowsers {

    IncognitoBrowser FIREFOX =
            new IncognitoBrowser(
                    "org.mozilla.firefox",
                    "org.mozilla.gecko.LauncherActivity",
                    2015503969,
                    "private_tab");

    IncognitoBrowser FENNEC =
            new IncognitoBrowser(
                    "org.mozilla.fennec_fdroid",
                    "org.mozilla.gecko.LauncherActivity",
                    550000,
                    "private_tab");

    // does not exist yet, but maybe one day...
    IncognitoBrowser ICECAT =
            new IncognitoBrowser(
                    "org.gnu.icecat",
                    "org.mozilla.gecko.LauncherActivity",
                    550000,
                    "private_tab");

    IncognitoBrowser JELLY =
            new IncognitoBrowser(
                    "org.lineageos.jelly",
                    "org.lineageos.jelly.MainActivity",
                    1,
                    "extra_incognito");
}


